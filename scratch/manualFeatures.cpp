#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/stitching.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <iostream>

using namespace std;
using namespace cv;

// fun green range: Scalar(45, 100, 50), Scalar(75, 255, 255)
// fun times blue range: Scalar(100, 100, 50), Scalar(140, 255, 255)

vector<Point2d> getThresholdPoints(cv::InputOutputArray img, const Scalar& min, const Scalar& max) {
    vector<vector<Point>> cts;
    vector<Point2d> centres;
    Mat hsv;
    cvtColor(img, hsv, CV_BGR2HSV);
    inRange(hsv, min, max, hsv);
    Mat element = getStructuringElement(MORPH_CROSS,
                    Size( 2*10 + 1, 2*10+1 ),
                    Point( 10, 10 ) );
    erode(hsv, hsv, element);
    findContours(hsv, cts, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    std::sort(cts.begin(), cts.end(), [](const vector<Point> a, const vector<Point> b) -> bool {
        double i = fabs(contourArea(Mat(a)));
        double j = fabs(contourArea(Mat(b)));
        return (i > j);
    });
    for(int i = 0; i<4; i++) {
        Moments m = moments(cts[i]);
        int centerX = (int)(m.m10 / m.m00);
        int centerY = (int)(m.m01 / m.m00);
        //circle(img, Point2d(centerX, centerY), 3, Scalar(0, 0, 255), -1);
        centres.push_back(Point2d(centerX, centerY));
    }
    std::sort(centres.begin(), centres.end(), [](const Point2d& a, const Point2d& b) -> bool {
        return a.y < b.y;
    });
    return centres;
}

int main(int argc, char** argv)
{
    Mat iRight = imread(argv[1]);
    Mat iFront = imread(argv[2]);
    Mat iLeft = imread(argv[3]);
    vector<Point2d> ptsLeft = getThresholdPoints(iLeft, Scalar(45, 100, 50), Scalar(75, 255, 255));
    vector<Point2d> ptsFrontLeft = getThresholdPoints(iFront, Scalar(45, 100, 50), Scalar(75, 255, 255));
    vector<Point2d> ptsFrontRight = getThresholdPoints(iFront, Scalar(100, 100, 50), Scalar(140, 255, 255));
    vector<Point2d> ptsRight = getThresholdPoints(iRight, Scalar(100, 100, 50), Scalar(140, 255, 255));
    Point2f flArr[] = {
        ptsFrontLeft[0],
        ptsFrontLeft[1],
        ptsFrontLeft[2],
        ptsFrontLeft[3]
    };
    Point2f lArr[] = {
        ptsLeft[0],
        ptsLeft[1],
        ptsLeft[2],
        ptsLeft[3]
    };
    Point2f frArr[] = {
        ptsFrontRight[0],
        ptsFrontRight[1],
        ptsFrontRight[2],
        ptsFrontRight[3]
    };
    Point2f rArr[] = {
        ptsRight[0],
        ptsRight[1],
        ptsRight[2],
        ptsRight[3]
    };
    Mat hL = getPerspectiveTransform(lArr, flArr);
    Mat resultL;
    warpPerspective(iLeft, resultL, hL, iFront.size());
    Mat hR = getPerspectiveTransform(rArr, frArr);
    Mat resultR;
    warpPerspective(iRight, resultR, hR, iFront.size());
    Mat mask = Mat::zeros(iFront.size(), CV_8U);
    // vector<Point2d> polygonVertices;
    // for (auto& pt : ptsFrontLeft) {
    //     polygonVertices.push_back(pt);
    // }
    // for (vector<Point2d>::reverse_iterator i = ptsFrontRight.rbegin(); 
    //     i != ptsFrontRight.rend(); ++i ) {
    //     polygonVertices.push_back(*i);
    // }
    // polygonVertices.push_back(ptsFrontLeft[0]);
    const Point polygonVertices[] = {
        ptsFrontLeft[0],
        ptsFrontLeft[1],
        ptsFrontLeft[2],
        ptsFrontLeft[3],
        ptsFrontRight[3],
        ptsFrontRight[2],
        ptsFrontRight[1],
        ptsFrontRight[0]
    };
    fillConvexPoly(mask, polygonVertices, 8, Scalar(255,255,255));
    //GaussianBlur(mask, mask, Size(101,101), 0);
    distanceTransform(255-mask, mask, CV_DIST_L2, 3);
    mask = mask/255.0;
    mask.setTo(cv::Scalar(1.0f), mask>1.0f);
    mask = 1.0f-mask;
    double min, max;
    minMaxLoc(mask, &min, &max);
    cout << "min max are " << min << ", " << max << endl;
    imwrite("mask.jpg", mask);
    iFront.convertTo(iFront, CV_32FC3);
    resultR.convertTo(resultR, CV_32FC3);
    resultL.convertTo(resultL, CV_32FC3);
    Mat blended;
    addWeighted(resultL, 0.5, resultR, 0.5, 0, blended);
    std::vector<cv::Mat> singleChannels;
    singleChannels.push_back(mask);
    singleChannels.push_back(mask);
    singleChannels.push_back(mask);
    merge(singleChannels, mask);
    mask.convertTo(mask, CV_32FC3);
    Mat blendedWithFront = iFront.mul(mask) + resultL.mul(Scalar(1.0f,1.0f,1.0f) - mask);
    blendedWithFront.convertTo(blendedWithFront, CV_8UC3);
    imwrite("blendo.jpg", blendedWithFront);

    return 0;
  
    // Mat pointCircles;
    // imgTwo.copyTo(pointCircles);
    // for (auto& pt : ptsTwo) {
    //     circle(pointCircles, pt, 3, Scalar(255, 0, 255, 0.9), -1);
    // }
    //cv::Mat h = findHomography(ptsOne, ptsTwo);
    // Mat result;
    // Mat h = getAffineTransform(pointsOneArray, pointsTwoArray);
    // cout << "h size is " << h.rows << "," << h.cols << endl;
    // //warpPerspective(imgTwo, result, h, imgOne.size());
    // warpAffine(imgTwo, result, h, imgOne.size());
    // imwrite("aligned.jpg", result);
    // // addWeighted(pointCircles, 0.8, imgTwo, 0.2, 0.0, imgTwo);
    // // imwrite("aligned.jpg", imgTwo);
    // return 0;
}