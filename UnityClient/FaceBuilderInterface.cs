﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Windows;

public class FaceBuilderInterface : MonoBehaviour
{
    // Import function interface
    [DllImport("FaceBuilder")]
    // landmarks - binary transformation data to be passed in
    // data      - empty byte array to be filled with mesh data
    // len       - size of the landmarks byte array; passed out as size of data byte array
    public static extern void GetMesh(byte[] landmarks, out byte[] data, out long len);
    
    // Sample code for mesh construction
    private void Start()
    {
        // Get byte array of binary transformation data (either file or stream)
        string path = "Assets/test.bin";
        byte[] landmarks = File.ReadAllBytes(path);
        // Get length of landmarks byte array (for use in dll)
        long itemCount = landmarks.Length;

        // Initialise array for mesh byte data
        // Initialised using fixed number 150,000 as native dll can't allocate C# array
        // May need to increase size for larger byte arrays (check itemCount after calling
        byte[] bytes = new byte[150000];
        
        // Call GetMesh function
        // and log the binary value stored at 7th byte (should be non-zero)
        GetMesh(landmarks, out bytes, out itemCount);
        Debug.Log(System.Convert.ToString(bytes[7], 2).PadLeft(8, '0'));
    }
}