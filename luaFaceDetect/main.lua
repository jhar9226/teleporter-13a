require 'camera'
require 'torch-ffmpeg'
require 'nnface'

function squareAtCenter(screenSize, rectSize)
  -- return: a table representing a square centered on the screen
  -- 
  local topLeft = {screenSize[3]/2.0 - rectSize/2.0, screenSize[2]/2.0 - rectSize/2.0}
  return {topLeft[1], topLeft[2], topLeft[1] + rectSize, topLeft[2] + rectSize}
end

local cam = image.Camera{idx=0, fps=30}
local frame = cam:forward()
local win
local rect = squareAtCenter(frame:size(), 400)
local rectCenter = {rect[1] + (rect[3] - rect[1])/2.0, rect[2] + (rect[4]-rect[2])/2.0}
local rectScaling = ((rect[3]-rect[1]+rect[4]-rect[2])/195)
-- model sourced from https://www.adrianbulat.com/downloads/FaceAlignment/2D-FAN-300W.t7
local detector = NNFace('models/2D-FAN-300W.t7', true)
while true do

  frame = cam:forward()
  image.drawRect(
    frame, 
    rect[1],rect[2],rect[3],rect[4],
    {lineWidth = 1, color={255,0,0},inplace=true}
  )
  frame = frame:sub(1,3,rect[2],rect[4],rect[1],rect[3])
  print(frame[{{}, 140,140}])
  detector:draw(frame, detector:detect(frame))
  win = image.display{win=win,image=frame}
end