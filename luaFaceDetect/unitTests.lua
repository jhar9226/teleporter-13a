require 'nnface'
require 'math'

-- model sourced from https://www.adrianbulat.com/downloads/FaceAlignment/2D-FAN-300W.t7
local detector = NNFace('models/2D-FAN-300W.t7', true)

-- Unit testing - uses the face photos in testdata/ and their .t7 expected face registration points
-- to calculate Mean Squared Error between the points detected and the points loaded from data.
-- 0.5 is used as threshold, though this is largely arbitrary.
if #arg == 0 then
print([[
    USAGE: unitTests.lua testdata/*.jpg (or whatever directory holds all your data).
    Data must be in the form of square photographs with accompanying .t7 tensors of face
    feature points, dimensions 1x68x2. The photo must have the same name as its points file,
    but .jpg instead of .t7.
]])
return
end

for i=1,#arg do
    dataFilename = arg[i]:gsub(".jpg", ".t7")
    img = image.load(arg[i],3,'byte')
    points=detector:detect(img:double()/255.0)
    data = torch.load(dataFilename)
    se = points:double() - data:double()
    se = se:cmul(se)
    mse = torch.mean(se)
    if (mse > 0.5) then
        print("TEST FAILED? Mean Squared Error is high between the loaded and detected data.")
    end
end