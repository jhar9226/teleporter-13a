do

require('cunn')
require('cudnn')
require 'torch'
require 'nn'
require 'nngraph'
require 'paths'
require 'image'

require 'bnn'
require 'optim'

torch.setheaptracking(true)
torch.setdefaulttensortype('torch.FloatTensor')
torch.setnumthreads(4)

local NNFace = torch.class('NNFace')

-- USAGE
-- in the main.lua, create a variable thus:
-- local detector = NNFace('path_to_model', useCudaOrNot)

function NNFace:__init(modelPath, loadCuda)
    self.model = torch.load(modelPath)
    if loadCuda then
        self.model = self.model:cuda()
    end
    self.model:evaluate()
end

function NNFace:detect(img)
    -- pass a SQUARE image tensor, and this will detect a face in it
    -- returns a 68,2 tensor corresponding to points of the face

    if img:size()[2] ~= img:size()[3] then
        print('input to detect must be a SQUARE image')
        return nil
    end

    -- scale the input tensor, send a 4d view of it to gpu
    local input = image.scale(img, 256, 256):cuda():view(1,3,256,256)
    -- ~*~*~*MAGIC HAPPENS IN THE NN~*~*~*~
    local output = self.model:forward(input)[4]:clone()
    -- get max activations by absolute pixel address (ignore first return var)
    local _, maxIndices = torch.max(output:view(output:size(1), output:size(2), output:size(3) * output:size(4)), 3)
    -- THE BELOW LINES UNTIL preds:add(-0.5) IS FROM THE ORIGINAL PAPER CODE
    -- AT https://github.com/1adrianb/2D-and-3D-face-alignment
    -- utils.lua, lines 94-110
    -- basically it's just turning the max indices back into x,y coords
    local preds = torch.repeatTensor(maxIndices, 1, 1, 2):float()
    -- get x coord in the 64x64 coords
    preds[{{}, {}, 1}]:apply(function(x) return (x - 1) % output:size(4) + 1 end)
    -- get y coord in the 64x64 coords
    preds[{{}, {}, 2}]:add(-1):div(output:size(3)):floor():add(1)
    for i = 1,preds:size(1) do        
        for j = 1,preds:size(2) do
            local hm = output[{i,j,{}}]
            local pX, pY = preds[{i,j,1}], preds[{i,j,2}]
            if pX > 1 and pX < 64 and pY > 1 and pY < 64 then
                local diff = torch.FloatTensor({hm[pY][pX+1]-hm[pY][pX-1], hm[pY+1][pX]-hm[pY-1][pX]})
                preds[i][j]:add(diff:sign():mul(.25))
            end
        end
    end
    preds:add(-0.5)
    return preds
end

function NNFace:draw(img, preds)
    -- utility func for drawing the detected points
    for i=1, 68 do
        local normX = preds[1][i][1]/64
        local normY = preds[1][i][2]/64
        local point = {img:size()[2]*normX, img:size()[2]*normY}
        image.drawRect(
          img, 
          point[1], point[2],
          point[1]+1, point[2]+1,
          {color = {255,0,0}, lineWidth = 2, inplace = true}
        )
      end
end

end