using System;
using UnityEngine;
using UnityEngine.Windows;

public class BinaryToObj
{

    public Vector3[] vertices;
    public Vector2[] UV;
    public int[] triangles;

    public void ConvertMain(byte[] bytes)
    {
        int num_vertex = BitConverter.ToInt32(bytes, 0);
        int num_triangles = BitConverter.ToInt32(bytes, 4);

        vertices = new Vector3[num_vertex];
        UV = new Vector2[num_vertex];
        triangles = new int[num_triangles*3];

        for (int i = 0; i < num_vertex; ++i)
        {
            vertices[i].x = BitConverter.ToSingle(bytes, 8 * (i + 1) + (i * 4));
            vertices[i].y = BitConverter.ToSingle(bytes, 8 * (i + 1) + (i * 4) + 4);
            vertices[i].z = BitConverter.ToSingle(bytes, 8 * (i + 1) + (i * 4) + 8);
        }

        for (int i = 0; i < num_vertex; ++i)
        {
            UV[i].x = BitConverter.ToSingle(bytes, 8 * (num_vertex + i + 1));
            UV[i].y = BitConverter.ToSingle(bytes, 8 * (num_vertex + i + 1) + 4);
        }

        
        for (int i = 0; i < num_triangles; ++i)
        {
            triangles[i * 3] = BitConverter.ToInt32(bytes, 8 * (num_vertex * 2 + i + 1) + ((num_vertex + i) * 4));
            triangles[i * 3 + 1] = BitConverter.ToInt32(bytes, 8 * (num_vertex * 2 + i + 1) + ((num_vertex + i) * 4) + 4);
            triangles[i * 3 + 2] = BitConverter.ToInt32(bytes, 8 * (num_vertex * 2 + i + 1) + ((num_vertex + i) * 4) + 8);
        }
    }

    public void ConvertVertexOnly(byte[] bytes)
    {
        int num_vertex = BitConverter.ToInt32(bytes, 0);

        vertices = new Vector3[num_vertex];

        for (int i = 0; i < num_vertex; ++i)
        {
            vertices[i].x = BitConverter.ToSingle(bytes, 8 * (i + 1) + (i * 4));
            vertices[i].y = BitConverter.ToSingle(bytes, 8 * (i + 1) + (i * 4) + 4);
            vertices[i].z = BitConverter.ToSingle(bytes, 8 * (i + 1) + (i * 4) + 8);
        }
    }
}