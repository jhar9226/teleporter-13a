/**
 * @file StreamUtils.h
 * @brief Provides a stream utility class for streaming binary data over.
 * 
 */
#ifndef STREAM_UTILS
#define STREAM_UTILS

#include <algorithm>
#include <sstream>

#ifdef _WIN32
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#pragma comment(lib, "Ws2_32.lib")
#else
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <unistd.h>
#endif

/**
 * @brief A stream utility class that provides functionality for streaming data.
 * 
 * Simply, this class provides functions to stream iostream data over the network.
 */
class StreamUtils
{
public:
	/**
	 * @brief Construct a new Stream Utils object.
	 * 
	 */
	StreamUtils();

	/**
	 * @brief Construct a new Stream Utils object.
	 * 
	 * @param port The port to be used for sending.
	 */
	StreamUtils(int port);

	/**
	 * @brief Destroy the Stream Utils object.
	 * 
	 */
	~StreamUtils();

	/**
	 * @brief Stream data to a connected client.
	 * 
	 * Blocks while listening for a client connection. Once connected
	 * the method streams all of the data in iostream over with the given
	 * size_buffer header.
	 * 
	 * @param data The iostream data to be sent.
	 * @param size_buffer The char byte array header to be sent.
	 * @return int Whether the data is sent successfully (0 or 1).
	 */
	int StreamData(std::iostream& data, char* size_buffer);

private:
	/**
	 * @brief Helper function to send data over to a given socket.
	 * 
	 * @param sock The socket to be sent over.
	 * @param data The iostream data to be sent.
	 * @param size_buffer The char byte array header to be sent.
	 * @return true The data is sent successfully.
	 * @return false The data is not sent successfully.
	 */
	bool SendData(int sock, std::iostream& data, char* size_buffer);

	/**
	 * @brief Helper function to send a buffer over to a given socket.
	 * 
	 * @param sock The socket to be sent over.
	 * @param buf The buffer to be sent.
	 * @param buflen The length of the buffer.
	 * @return true The buffer is sent successfully.
	 * @return false The buffer is not sent successfully.
	 */
	bool SendBuffer(int sock, void * buf, int buflen);

	int ListenSocket, ClientSocket;
	struct sockaddr_in server, client;
};

#endif // STREAM_UTILS
