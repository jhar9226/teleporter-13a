#include "FaceOff.h"

FaceOff::FaceOff(std::string morphable_model,
	std::string ibug_mapping,
	std::string model_contour,
	std::string blendshapes,
	std::string edge_topology,
	std::string landmark_detector,
	std::string face_detector) :
	morphable_model(eos::morphablemodel::load_model(morphable_model)),
	landmark_mapper(eos::core::LandmarkMapper(ibug_mapping)),
	model_contour(eos::fitting::ModelContour::load(model_contour)),
	ibug_contour(eos::fitting::ContourLandmarks::load(ibug_mapping)),
	blendshapes(eos::morphablemodel::load_blendshapes(blendshapes)),
	edge_topology(eos::morphablemodel::load_edge_topology(edge_topology))
{
	// Load the landmark detection model:
	try {
		rcr_model = rcr::load_detection_model(landmark_detector);
	}
	catch (const cereal::Exception& e) {
		throw "Error reading the RCR model" + landmark_detector + ": " + e.what();
	}

	// Load the face detector from OpenCV:
	if (!face_cascade.load(face_detector))
	{
		throw "Error loading the face detector " + face_detector + ".";
	}


}

FaceOff::~FaceOff()
{
}

int FaceOff::Run(bool save_mesh = true, bool show_mesh = true, bool streamable = true, int port = 9206)
{
	cv::VideoCapture cap;
	cap.open(0);
	if (!cap.isOpened()) 
	{
		throw "Couldn't open the camera.";
	}

	StreamUtils stream;
	if (streamable)
	{
		stream = StreamUtils(port);
	}

	if (show_mesh)
	{
		cv::namedWindow("render", cv::WINDOW_NORMAL);
	}

	bool have_face = false;
	rcr::LandmarkCollection<cv::Vec2f> current_landmarks;
	cv::Mat frame;
	PcaCoefficientMerging pca_shape_merging;

	int frame_num = 0;
	// Loop through camera feed
	while (true)
	{
		cap >> frame;
		if (frame.empty())
		{
			break;
		}

		if (!have_face) 
		{ // If we do not have a previous face frame:
			// Run the face detector and obtain the initial estimate using the mean landmarks:
			std::vector<cv::Rect> detected_faces;
			face_cascade.detectMultiScale(frame, detected_faces, 1.2, 2, 0, cv::Size(110, 110));

			// Rescale the V&J facebox to make it more like an ibug-facebox:
			// (also make sure the bounding box is square, V&J's is square)
			cv::Rect ibug_facebox = rescale_facebox(detected_faces[0], 0.85, 0.2);

			// Predicted landmarks
			current_landmarks = rcr_model.detect(frame, ibug_facebox);

			have_face = true;
		}
		else 
		{ // We have a previous face frame:
			// Track and initialise using the enclosing bounding
			// box from the landmarks from the last frame:
			auto enclosing_bbox = get_enclosing_bbox(rcr::to_row(current_landmarks));
			enclosing_bbox = make_bbox_square(enclosing_bbox);
			current_landmarks = rcr_model.detect(frame, enclosing_bbox);
		}

		// Fit the 3DMM:
		eos::fitting::RenderingParameters rendering_params;
		std::vector<float> shape_coefficients, blendshape_coefficients;
		std::vector<Eigen::Vector2f> image_points;
		eos::core::Mesh mesh;
		std::tie(mesh, rendering_params) = eos::fitting::fit_shape_and_pose(morphable_model, blendshapes, rcr_to_eos_landmark_collection(current_landmarks), landmark_mapper, 
			                                                                frame.cols, frame.rows, edge_topology, ibug_contour, model_contour, 3, 5, 15.0f, 
			                                                                eos::cpp17::nullopt, shape_coefficients, blendshape_coefficients, image_points);

		// Extract the texture using the fitted mesh from this frame:
		const Eigen::Matrix<float, 3, 4> affine_cam = eos::fitting::get_3x4_affine_camera_matrix(rendering_params, frame.cols, frame.rows);
		const cv::Mat isomap = eos::core::to_mat(eos::render::extract_texture(mesh, affine_cam, eos::core::from_mat(frame), false, eos::render::TextureInterpolation::NearestNeighbour, 1024));

		if (streamable)
		{
			int iResult = StreamLandmarks(stream, current_landmarks, isomap);
			if (iResult == EXIT_FAILURE)
			{
				return EXIT_FAILURE;
			}
		}

		if (show_mesh || save_mesh)
		{
			// Merge face mesh
			shape_coefficients = pca_shape_merging.add_and_merge(shape_coefficients);
			const Eigen::VectorXf merged_shape = morphable_model.get_shape_model().draw_sample(shape_coefficients) +
                                                 to_matrix(blendshapes) *
                                                 Eigen::Map<const Eigen::VectorXf>(blendshape_coefficients.data(), blendshape_coefficients.size());
			const eos::core::Mesh merged_mesh = eos::morphablemodel::sample_to_mesh(merged_shape, morphable_model.get_color_model().get_mean(), morphable_model.get_shape_model().get_triangle_list(),
				                                                                    morphable_model.get_color_model().get_triangle_list(), morphable_model.get_texture_coordinates());
			if (save_mesh)
			{
				SaveMesh(frame_num, merged_mesh, isomap, current_landmarks, true, true);
				++frame_num;
			}

			if (show_mesh)
			{
				ShowMesh(rendering_params, merged_mesh, isomap);
			}
		}
	}

	return EXIT_SUCCESS;
}

int FaceOff::StreamLandmarks(StreamUtils stream, rcr::LandmarkCollection<cv::Vec2f> landmarks, cv::Mat texture)
{
	// Use Cereal portable library to quickly serialise landmark data
	// into binary to be sent over the network
	std::stringstream ss;
	{
		cereal::PortableBinaryOutputArchive oarchive(ss);

		oarchive(landmarks);
	}
	// We also need to append texture file at end of stream
	std::vector<uchar> buf;
	cv::imencode(".jpg", texture, buf);
	std::copy(buf.cbegin(), buf.cend(), std::ostream_iterator<uchar>(ss));

	// Sending requires header file to distinguish
	// file size and size of image
	ss.seekg(0, std::ios::end);
	int filesize = ss.tellg();
	ss.seekg(0, std::ios::beg);

	int img_size = buf.size();

	char size_buffer[8];
	memcpy(size_buffer, &filesize, sizeof(filesize));
	memcpy(&size_buffer[4], &img_size, sizeof(img_size));

	// Finally send the data
	return stream.StreamData(ss, size_buffer);
}

void FaceOff::SaveMesh(int frame_num, eos::core::Mesh mesh, cv::Mat texture, rcr::LandmarkCollection<cv::Vec2f> landmarks, bool save_obj, bool save_landmarks)
{
	if (save_landmarks)
	{
		std::ofstream os("output/landmarks/" + std::to_string(frame_num) + ".bin", std::ios::binary);
		cereal::PortableBinaryOutputArchive oarchive(os);

		oarchive(landmarks);
	}

	if (save_obj)
	{
		eos::core::write_textured_obj(mesh, "output/wavefront/" + std::to_string(frame_num) + ".obj");
		cv::imwrite("output/wavefront/" + std::to_string(frame_num) + ".jpg", texture);
	}
}

void FaceOff::ShowMesh(eos::fitting::RenderingParameters rendering_params, eos::core::Mesh mesh, cv::Mat texture)
{
	eos::core::Image4u rendering;
	auto modelview_no_translation = rendering_params.get_modelview();
	modelview_no_translation[3][0] = 0;
	modelview_no_translation[3][1] = 0;
	std::tie(rendering, std::ignore) = eos::render::render(mesh, modelview_no_translation, glm::ortho(-130.0f, 130.0f, -130.0f, 130.0f), 256, 256, eos::render::create_mipmapped_texture(texture), true, false, false);
	cv::resizeWindow("render", 600, 600);
	cv::imshow("render", eos::core::to_mat(rendering));
	cv::waitKey(1);
}
