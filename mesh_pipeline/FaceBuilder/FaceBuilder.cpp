#include "FaceBuilder.h"
using namespace eos;
namespace po = boost::program_options;
namespace fs = boost::filesystem;
using cv::Mat;
using cv::Vec2f;
using cv::Vec3f;
using cv::Vec4f;
using cv::Rect;
using std::cout;
using std::endl;
using std::vector;
using std::string;

extern "C" {
	FACEBUILDER_API void GetMesh(unsigned char** landmarks, unsigned char **data, long* len) {
		// Load the Morphable Model and the LandmarkMapper:
		const morphablemodel::MorphableModel morphable_model = morphablemodel::load_model("./Assets/share/sfm_shape_3448.bin");
		const core::LandmarkMapper landmark_mapper = core::LandmarkMapper("./Assets/share/ibug_to_sfm.txt");

		const fitting::ModelContour model_contour = fitting::ModelContour::load("./Assets/share/sfm_model_contours.json");
		const fitting::ContourLandmarks ibug_contour = fitting::ContourLandmarks::load("./Assets/share/ibug_to_sfm.txt");

		const morphablemodel::Blendshapes blendshapes = morphablemodel::load_blendshapes("./Assets/share/expression_blendshapes_3448.bin");

		const morphablemodel::EdgeTopology edge_topology = morphablemodel::load_edge_topology("./Assets/share/sfm_3448_edge_topology.json");
		
		rcr::LandmarkCollection<cv::Vec2f> current_landmarks;
		{
			std::stringstream s;
			s.write((char *)landmarks, *len);
			cereal::BinaryInputArchive iarchive(s);
			iarchive(current_landmarks);
		}

		// Fit the 3DMM:
		fitting::RenderingParameters rendering_params;
		vector<float> shape_coefficients, blendshape_coefficients;
		vector<Eigen::Vector2f> image_points;
		core::Mesh mesh;
		std::tie(mesh, rendering_params) = fitting::fit_shape_and_pose(morphable_model, blendshapes, rcr_to_eos_landmark_collection(current_landmarks),
			landmark_mapper, 1920, 1080, edge_topology,
			ibug_contour, model_contour, 3, 5, 15.0f, cpp17::nullopt, shape_coefficients,
			blendshape_coefficients, image_points);

		// Write to byte array
		std::vector<unsigned char> bytes;
		int a = mesh.vertices.size();
		int b = mesh.tvi.size();

		bytes.push_back(a >> 24);
		bytes.push_back(a >> 16);
		bytes.push_back(a >> 8);
		bytes.push_back(a);

		bytes.push_back(b >> 24);
		bytes.push_back(b >> 16);
		bytes.push_back(b >> 8);
		bytes.push_back(b);

		for (Eigen::Vector3f v : mesh.vertices)
		{
			uint32_t i0 = reinterpret_cast<uint32_t&>(v[0]);
			uint32_t i1 = reinterpret_cast<uint32_t&>(v[1]);
			uint32_t i2 = reinterpret_cast<uint32_t&>(v[2]);

			bytes.push_back(i0 >> 24);
			bytes.push_back(i0 >> 16);
			bytes.push_back(i0 >> 8);
			bytes.push_back(i0);
			bytes.push_back(i1 >> 24);
			bytes.push_back(i1 >> 16);
			bytes.push_back(i1 >> 8);
			bytes.push_back(i1);
			bytes.push_back(i2 >> 24);
			bytes.push_back(i2 >> 16);
			bytes.push_back(i2 >> 8);
			bytes.push_back(i2);
		}

		for (Eigen::Vector2f uv : mesh.texcoords)
		{
			float temp = 1.0f - uv[1];
			uint32_t i0 = reinterpret_cast<uint32_t&>(uv[0]);
			uint32_t i1 = reinterpret_cast<uint32_t&>(temp);

			bytes.push_back(i0 >> 24);
			bytes.push_back(i0 >> 16);
			bytes.push_back(i0 >> 8);
			bytes.push_back(i0);
			bytes.push_back(i1 >> 24);
			bytes.push_back(i1 >> 16);
			bytes.push_back(i1 >> 8);
			bytes.push_back(i1);
		}

		for (std::array<int, 3> idx : mesh.tvi)
		{
			for (int i : idx)
			{
				bytes.push_back(i >> 24);
				bytes.push_back(i >> 16);
				bytes.push_back(i >> 8);
				bytes.push_back(i);
			}
		}

		// Save to parameter for C#
		*len = bytes.size();
		auto size = (*len) * sizeof(unsigned char);

		*data = static_cast<unsigned char*>(CoTaskMemAlloc(size));
		memcpy(*data, bytes.data(), size);
	}
}