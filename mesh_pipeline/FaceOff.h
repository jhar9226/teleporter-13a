/**
 * @file FaceOff.h
 * @brief The FaceOff class provides functionality to recreate a 3D face from 2D input.
 * 
 * Acknowledgement goes to Patrik Huber's 4dface https://github.com/patrikhuber/4dface
 * and his library eos https://github.com/patrikhuber/eos
 * 
 */
#ifndef FACE_OFF
#define FACE_OFF

#include <iostream>
#include <string>
#include <vector>

#include "StreamUtils.h"

#include "cereal/archives/portable_binary.hpp"
#include "eos/core/Image_opencv_interop.hpp"
#include "eos/core/Landmark.hpp"
#include "eos/core/LandmarkMapper.hpp"
#include "eos/fitting/fitting.hpp"
#include "eos/morphablemodel/MorphableModel.hpp"
#include "eos/render/draw_utils.hpp"
#include "eos/render/render.hpp"
#include "eos/render/texture_extraction.hpp"
#include "helpers.hpp"
#include "opencv2/core/core.hpp"
#include "rcr/model.hpp"

/**
 * @brief The FaceOff class provides functionality to recreate a 3D face from 2D input.
 * 
 * Extracts the face from a camera and recreates a 3D mesh from it, including the texture.
 * Also includes streaming capabilities to stream the mesh over the network.
 * The mesh streaming only streams landmark data, as specified by the client. 
 * This offloads work on the network by decreasing the amount of data sent. 
 * Instead the mesh is created by the FaceBuilder API on the client's side.
 */
class FaceOff
{
public:
	/**
	 * @brief Construct a new Face Off object
	 * 
	 * @param morphable_model Path to binary morphable model data.
	 * @param ibug_mapping Path to iBUG facial mapping to model data.
	 * @param model_contour Path to model contours data.
	 * @param blendshapes Path to model blendshapes data.
	 * @param edge_topology Path to model edge topology data.
	 * @param landmark_detector Path to 68 landmarks detector data.
	 * @param face_detector Path to haarcascade face detector data.
	 */
	FaceOff(std::string morphable_model,
            std::string ibug_mapping,
            std::string model_contour,
            std::string blendshapes,
            std::string edge_topology,
            std::string landmark_detector,
            std::string face_detector);
		
	/**
	 * @brief Destroy the Face Off object
	 * 
	 */
	virtual ~FaceOff();

	/**
	 * @brief Runs the face extractor.
	 * 
	 * @param save_mesh Whether the mesh is saved locally (with sequential naming).
	 * @param show_mesh Whether the mesh is seen locally (for debugging purposes).
	 * @param streamable Whether the mesh is streamed over the network.
	 * @param port The port to be used if the mesh is to be streamed.
	 * @return int Whether the program has run successfully (0 or 1).
	 */
	int Run(bool save_mesh = true, bool show_mesh = true, bool streamable = true, int port = 9206);

private:
	/**
	 * @brief Helper function to stream the landmarks data.
	 * 
	 * Converts landmarks data and texture into binary stream to be used by StreamUtils object.
	 * 
	 * @param stream The StreamUtils object to be used.
	 * @param landmarks The landmarks data to be streamed.
	 * @param texture The texture data to be streamed.
	 * @return int Whether the data has been successfully sent (0 or 1).
	 */
	int StreamLandmarks(StreamUtils stream, rcr::LandmarkCollection<cv::Vec2f> landmarks, cv::Mat texture);

	/**
	 * @brief Helper function to save the mesh locally.
	 * 
	 * Can save the mesh and texture (OBJ and JPG), and/or the landmarks data (in binary format).
	 * Saving landmarks data is typically used for debugging purposes.
	 * 
	 * @param frame_num The frame_num for the mesh (for sequential numbering).
	 * @param mesh The mesh to be saved.
	 * @param texture The texture to be saved.
	 * @param landmarks The landmarks to be saved.
	 * @param save_obj Whether to save as OBJ and JPG.
	 * @param save_landmarks WHether to save as landmarks binary data.
	 */
	void SaveMesh(int frame_num, eos::core::Mesh mesh, cv::Mat texture, rcr::LandmarkCollection<cv::Vec2f> landmarks, bool save_obj, bool save_landmarks);

	/**
	 * @brief Helper function to show the mesh locally in real-time.
	 * 
	 * @param rendering_params The rendering parameters matrix so the object is centered. 
	 * @param mesh The mesh to be shown.
	 * @param texture The texture to be shown.
	 */
	void ShowMesh(eos::fitting::RenderingParameters rendering_params, eos::core::Mesh mesh, cv::Mat texture);

	const eos::morphablemodel::MorphableModel morphable_model;
	const eos::core::LandmarkMapper landmark_mapper;
	const eos::fitting::ModelContour model_contour;
	const eos::fitting::ContourLandmarks ibug_contour;
	const eos::morphablemodel::Blendshapes blendshapes;
	const eos::morphablemodel::EdgeTopology edge_topology;
	rcr::detection_model rcr_model;
	cv::CascadeClassifier face_cascade;
};

#endif // FACE_OFF
