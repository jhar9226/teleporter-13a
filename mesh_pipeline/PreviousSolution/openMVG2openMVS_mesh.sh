#!/bin/sh
#! -*- encoding: utf-8 -*-

# Shell script pipeline using openMVG and openMVS
#
# usage : ./openMVG2openMVS_mesh.sh
#

echo "START"

# openMVG binary location
OPENMVG="./openMVG"
# openMVS binary location
OPENMVS="./openMVS"

# camera database list
CAMERAS="${OPENMVG}/sensor_width_database/sensor_width_camera_database.txt"

# Input images folder location
INPUT="./input"
# Output mesh folder location
OUTPUT="./output"

##############################
# openMVG scene reconstruction
##############################

MATCH_DIR="${OUTPUT}/matches"
SCENE_DIR="${OUTPUT}/reconstruction_sequential"

mkdir -p $MATCH_DIR
mkdir -p $SCENE_DIR

echo "1. Initialise Image Listing"
"${openMVG}/openMVG_main_SfMInit_ImageListing" -i $INPUT -o $OUTPUT -d $CAMERAS -c 3 -f 4377

echo "2. Compute Features"
"${openMVG}/openMVG_main_ComputeFeatures" -i "${MATCH_DIR}/sfm_data.json" -o $MATCH_DIR -p ULTRA -m AKAZE_FLOAT -f 1

echo "3. Compute Matches"
"${openMVG}/openMVG_main_ComputeMatches" -i "${MATCH_DIR}/sfm_data.json" -o $MATCH_DIR -f 1 -n ANNL2 -g h

echo "4. Sequential Reconstruction"
"${openMVG}/openMVG_main_IncrementalSfM" -i "${MATCH_DIR}/sfm_data.json" -m $MATCH_DIR -o $SCENE_DIR

##############################
# openMVG to openMVS
##############################

echo "5. Conversion openMVG to openMVS"
"${openMVG}/openMVG_main_openMVG2openMVS" -i "${SCENE_DIR}/sfm_data.bin" -o "${OUTPUT}/scene.mvs"

##############################
# openMVS mesh reconstruction
##############################

echo "6. Densify Point-Cloud"
"${OPENMVS}/DensifyPointCloud" -i "${OUTPUT}/scene.mvs" -o "${OUTPUT}/dense_scene.mvs"

echo "7. Reconstruct Mesh"
"${OPENMVS}/ReconstructMesh" -i "${OUTPUT}/dense_scene.mvs" -o "${OUTPUT}/mesh.mvs"

echo "8. Refine Mesh"
"${OPENMVS}/RefineMesh" -i "${OUTPUT}/mesh.mvs" -o "${OUTPUT}/refined_mesh.mvs"

echo "9. Texturise Mesh"
"${OPENMVS}/TextureMesh" -i "${OUTPUT}/refined_mesh.mvs" -o "${OUTPUT}/textured_mesh.mvs" --export-type=obj

echo "DONE"