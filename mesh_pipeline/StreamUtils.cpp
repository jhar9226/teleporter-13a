#include "StreamUtils.h"

StreamUtils::StreamUtils()
{
}

StreamUtils::StreamUtils(int port)
{
	#ifdef _WIN32
		WSADATA Winsock;
		int iResult;
		iResult = WSAStartup(MAKEWORD(2, 2), &Winsock);
		// Initialise Winsock
		if (iResult != 0) {
			throw "WSAStartup failed: " + iResult;
		}
	#endif

	memset(&server, 0, sizeof(server));
	memset(&client, 0, sizeof(client));
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);
	server.sin_family = AF_INET;

	ListenSocket = -1;
	ClientSocket = -1;

	// Create a SOCKET for the server to listen for client connections
	ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (ListenSocket < 0) {
		#ifdef _WIN32
			printf("Error at socket(): %ld\n", WSAGetLastError());
			WSACleanup();
		#endif
		throw "Error at socket()";
	}
	printf("Created Socket\n");

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, (struct sockaddr*)&server, sizeof(server));
	if (iResult < 0) {
		#ifdef _WIN32
			printf("Bind failed with error: %d\n", WSAGetLastError());
		#endif
		closesocket(ListenSocket);
		#ifdef _WIN32
			WSACleanup();
		#endif
		throw "Bind failed with error";
	}
	printf("Bind Socket\n");
}


StreamUtils::~StreamUtils()
{
	closesocket(ClientSocket);
	closesocket(ListenSocket);
	#ifdef _WIN32
		WSACleanup();
	#endif
}

int StreamUtils::StreamData(std::iostream& data, char* size_buffer)
{
	if (ClientSocket < 0) {
		if (listen(ListenSocket, SOMAXCONN) < 0) {
			#ifdef _WIN32
				printf("Listen failed with error: %ld\n", WSAGetLastError());
			#endif
			closesocket(ListenSocket);
			#ifdef _WIN32
				WSACleanup();
			#endif
			return EXIT_FAILURE;
		}

		// Accept a client socket
		int client_size = sizeof(client);
		ClientSocket = accept(ListenSocket, (struct sockaddr*) &client, (int*)&client_size);;
		if (ClientSocket < 0) {
			#ifdef _WIN32
				printf("accept failed: %d\n", WSAGetLastError());
			#endif
			closesocket(ListenSocket);
			#ifdef _WIN32
				WSACleanup();
			#endif
			return EXIT_FAILURE;
		}
		printf("Client Socket accepted\n");
	}

	printf("Reading data\n");
	if (!SendData(ClientSocket, data, size_buffer))
	{
		printf("Error sending data\n");
		closesocket(ClientSocket);
		closesocket(ListenSocket);
		#ifdef _WIN32
			WSACleanup();
		#endif
		return EXIT_FAILURE;
	}
	printf("Sent data\n");
	return EXIT_SUCCESS;
}

bool StreamUtils::SendData(int sock, std::iostream& data, char* size_buffer)
{
	printf("Check filesize\n");
	data.seekg(0, std::ios::end);
	int filesize = data.tellg();
	printf("Filesize: %i\n", filesize);
	data.seekg(0, std::ios::beg);
	if (filesize == EOF)
		return false;

	// Send header first
	if (!SendBuffer(sock, size_buffer, sizeof(size_buffer)))
	{
		printf("Header error\n");
		return false;
	}
	printf("sent header\n");

	if (filesize > 0)
	{
		// Loop until all data is sent
		char buffer[8192];
		do
		{
			printf("%i\n", filesize);
			size_t num = (std::min)((unsigned long long)filesize, sizeof(buffer));
			data.read(buffer, num);
			if (!data) 
			{
				printf("Data stream error\n");
				return false;
			}
			if (!SendBuffer(sock, buffer, num))
			{
				printf("Data error\n");
				return false;
			}
			printf("Sent a buffer\n");
			filesize -= num;
		} while (filesize > 0);
	}
	return true;
}

bool StreamUtils::SendBuffer(int sock, void *buf, int buflen)
{
	char *pbuf = (char *)buf;

	// Loop through buffer
	// in case buffer isn't entirely sent
	// and is split into packets
	while (buflen > 0)
	{
		int num = send(sock, pbuf, buflen, 0);
		if (num < 0)
		{
			#ifdef _WIN32
				if (WSAGetLastError() == WSAEWOULDBLOCK)
				{
					printf("Blocking error, will retry\n");
					continue;
				}
				printf("Failed: %d\n", WSAGetLastError());
			#endif
			return false;
		}

		pbuf += num;
		buflen -= num;
	}

	return true;
};
