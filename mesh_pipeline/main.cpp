#include "FaceOff.h"

int main()
{
	FaceOff faceoff("../share/sfm_shape_3448.bin",
					"../share/ibug_to_sfm.txt",
					"../share/sfm_model_contours.json",
					"../share/expression_blendshapes_3448.bin",
					"../share/sfm_3448_edge_topology.json",
					"../share/face_landmarks_model_rcr_68.bin",
					"../share/haarcascade_frontalface_alt2.xml");

	return faceoff.Run();
}