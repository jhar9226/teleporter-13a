#include "PointFinderContour.hpp"

using namespace cv;
using namespace std;

PointFinderContour::~PointFinderContour() {}

vector<Point2d> PointFinderContour::findPoints(InputArray img, int maxPoints) {
    vector<vector<Point>> cts;
    vector<Point2d> centres;
    Mat hsv;
    cvtColor(img, hsv, CV_BGR2HSV);
    inRange(hsv, this->hsvMin, this->hsvMax, hsv);
    Mat element = getStructuringElement(MORPH_CROSS,
                    Size( 2*10 + 1, 2*10+1 ),
                    Point( 10, 10 ) );
    erode(hsv, hsv, element); // TODO is this necessary?
    findContours(hsv, cts, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    std::sort(cts.begin(), cts.end(), [](const vector<Point> a, const vector<Point> b) -> bool {
        double i = fabs(contourArea(Mat(a)));
        double j = fabs(contourArea(Mat(b)));
        return (i > j);
    });
    for(int i = 0; i<4; i++) {
        Moments m = moments(cts[i]);
        int centerX = (int)(m.m10 / m.m00);
        int centerY = (int)(m.m01 / m.m00);
        //circle(img, Point2d(centerX, centerY), 3, Scalar(0, 0, 255), -1);
        centres.push_back(Point2d(centerX, centerY));
    }
    std::sort(centres.begin(), centres.end(), [](const Point2d& a, const Point2d& b) -> bool {
        return a.y < b.y;
    });
    return centres;
}