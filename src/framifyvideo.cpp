#include <sstream>
#include <chrono>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
    if (argc < 2) {
        cout << R"(
Usage:
    arg1: device num, should be 0
    arg2: read every nth frame, eg 2 would read every second frame. Default is 1 ie every frame read
    arg3: float, seconds to capture, defaults to -1.0, which records until ESC is pressed
        )" << endl;
        exit(1);
    }
    int frameSkip = 1;
    float duration = -1.0f;
    if (argc > 2) {
        frameSkip = atoi(argv[2]);
        if (frameSkip <= 0) {
            frameSkip = 1;
        }
    }
    if (argc > 3) {
        duration = atof(argv[3]);
        if (duration < 0.0f) {
            duration = 10000.0f;// oh what, you're going to record more than 10,000 seconds? Nah m8 nah
        }
    }
    chrono::time_point<chrono::system_clock> end = chrono::system_clock::now() + chrono::milliseconds((int)duration*1000);;
    Mat frame;
    VideoCapture cap = VideoCapture(atoi(argv[1]));
    if (!cap.isOpened()) {
		cout << "Video cannot be opened." << endl;
    	return 1;
    }
    int frameNum = 0;
	while(true) {
        if (std::chrono::system_clock::now() > end) {
            break;
        }
		if(!cap.read(frame)) {
			cout << "can no longer read video stream! exiting." << endl;
			break;
		}
        frameNum++;
        imshow("recording", frame);
        if (frameNum % frameSkip != 0) {
            continue;
        }
        ostringstream oss;
        oss << "framecaptures/frame" << frameNum << ".jpg";
		imwrite(oss.str(), frame);
		if (waitKey(30) == 27) {
			break;
		}
	}
}