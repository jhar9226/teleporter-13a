### Prepare sample .OBJ files for UI team to benchmark test with
- Priority: High
- Assignee: @richgao
- Status: Complete

### Research facial stitching
- Priority: High
- Assignee: @richgao, @ahar0137, @jhar9226
- Status: Complete

### Research facial mesh creation
- Priority: High
- Assignee: @richgao, @jhar9226
- Status: Complete

### Develop OpenCV pipeline for still images
- Priority: High
- Assignee: @ahar0137
- Status: Complete

### Develop OpenCV pipeline for video streams
- Priority: Medium
- Assignee: @ahar0137
- Status: To Do

### Develop Facial Stitching module
- Priority: High
- Assignee: @ahar0137
- Status: In Progress

### Develop Mesh Creation module
- Priority: High
- Assignee: @richgao
- Status: In Progress

### Research methods to synchronise multiple video streams
- Priority: Medium
- Assignee: @chrishg2001, @jhar9226
- Status: In Progress

### Develop video stream synchronisation module
- Priority: Low
- Assignee: @chrishg2001
- Status: Low

### Research camera hardware requirements
- Priority: High
- Assignee: @jhar9226
- Status: Complete

### Order camera hardware
- Priority: High
- Assignee: @jhar9226
- Status: Complete

### Build camera rig
- Priority: High
- Assignee: @jhar9226
- Status: In Progress

### Research video streaming capabilities over USB or other interfaces
- Priority: High
- Assignee: @jhar9226
- Status: In Progress

### Develop video stream hardware interface
- Priority: Medium
- Assignee: @jhar9226
- Status: In Progress
