#pragma once

#include <vector>
#include <opencv2/opencv.hpp>
#include "PointFinder.hpp"

/**
 * Implementation of PointFinder for finding points of a certain colour
 * in an image.
 */
class PointFinderContour : public PointFinder {
public:

    ~PointFinderContour();
    /**
     * Finds points at center of mass of contours of colours defined by hsvMin and Max
     * @param img the image to find points in
     * @param maxPoints maximum number of points to keep, -1 for keeping all points
     */
    std::vector<cv::Point2d> findPoints(cv::InputArray img, int maxPoints = -1) override;

    cv::Scalar hsvMin; //!< minimum hsv val to match on
    cv::Scalar hsvMax; //!< max hsv val to match on
private:
};