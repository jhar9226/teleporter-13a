#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

/**
 * PointFinder an interface for finding points of interest inside an image
 */
class PointFinder {
public:
    virtual ~PointFinder() {}
    /**
     * interface for finding points arbitrarily in an image
     * @param img the image to find points in
     * @param maxPoints maximum number of points to keep, -1 for keeping all points
     */
    virtual std::vector<cv::Point2d> findPoints(cv::InputArray img, int maxPoints = -1) = 0;
};