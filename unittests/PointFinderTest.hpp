#include "PointFinder.hpp"
#include <vector>
#include <assert.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class PointFinderTest {
public:
    void runStandardTests(PointFinder& pf);
    void runStandardTests(PointFinder& pf, Mat material);
    void runStandardTests(PointFinder& pf, Mat material, int numPoints);
};

void PointFinderTest::runStandardTests(PointFinder& pf) {
    // try empty img obj
    Mat blank;
    try {
        pf.findPoints(blank);
    } catch (exception e) {
        cout << "Test on empty img obj FAILED:" << e.what() << endl;
    }

    // try blank img
    Mat empty = cv::Mat::zeros(cv::Size(1000, 1000), CV_64FC1);
    vector<Point2d> pointsBlank;
    try {
        pointsBlank = pf.findPoints(blank, 1);
    } catch (exception e) {
        cout << "Test on blank img obj FAILED:" << e.what() << endl;
    }
    assert(pointsBlank.size() <= 1);
}

void PointFinderTest::runStandardTests(PointFinder& pf, Mat material) {
    this->runStandardTests(pf);
    vector<Point2d> pointsImg;
    try {
        pointsImg = pf.findPoints(material, 1);
    } catch (exception e) {
        cout << "Test on provided img obj FAILED:" << e.what() << endl;
    }
    assert(pointsImg.size() <= 1);
}

void PointFinderTest::runStandardTests(PointFinder& pf, Mat material, int numPoints) {
    this->runStandardTests(pf, material);
    vector<Point2d> pointsImg;
    try {
        pointsImg = pf.findPoints(material);
    } catch (exception e) {
        cout << "Test on provided img obj with provided numPoints FAILED:" << e.what() << endl;
    }
    assert(pointsImg.size() == numPoints);
}