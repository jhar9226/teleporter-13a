#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char *argv[])
{
    VideoCapture video(0);

    // Check if camera opened successfully
    if(!video.isOpened()){
      cout << "Error opening video stream or file" << endl;
      return -1;
    }

    FILE *pPipe;
    stringstream sstm;

    sstm << "./ffmpeg -i pipe:0 -c:v libx264 -preset ultrafast -f rtp_mpegts rtp://127.0.0.1:1234";

    if ( !(pPipe = popen(sstm.str().c_str(), "w")) ) {
        cout << "popen error" << endl;
        exit(1);
    }

    while(true){
        Mat frame;
        video >> frame;
        std::vector<uchar> buff; //buffer for coding
        std::vector<int> param(2);
        param[0] = cv::IMWRITE_JPEG_QUALITY;
        param[1] = 80; //default(95) 0-100
        cv::imencode(".jpg", frame, buff, param);

        fwrite(&buff[0], sizeof(uchar), buff.size(), pPipe);
        fflush(pPipe);

    }

    fclose(pPipe);
}

