#ifndef READVIDEO_H
#define READVIDEO_H

#include <iostream>
#include <vector>
#include <list>
#include <opencv2/opencv.hpp>

/**
 * Class which takes a vector of video source files,
 * synchronises the data and outputs it as an array
 * of image matrices
 */

using namespace std;
using namespace cv;

class ReadVideo
{
public:
    ReadVideo(const vector<int> &videos);
    ReadVideo(const vector<String> &videos);

    /**
     * @brief syncStream synchronises video streams
     * @return 0 on success, -1 on failure
     */
    int syncStream();

    /**
     * @brief getStreams gets a vector of the next
     * synchronised frames across all camera inputs
     * @return vector of image matrices
     */
    vector<shared_ptr<Mat>> getStreams();
private:
    vector<shared_ptr<VideoCapture>> feedArray;
    vector<list<shared_ptr<Mat>>> syncedStreams; //vector storing list of synchronised streams
    bool allStreamsHaveSynced = false;
};


#endif // READVIDEO_H

