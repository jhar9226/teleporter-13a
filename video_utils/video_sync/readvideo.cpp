#include "readvideo.h"

int LUMINENCE_CONSTANT = 15; //how much the luminence needs to increase by to register a flashing light

//calculate luminence given rgba values
double luminenceCalculator(int r, int g, int b){
    return 0.299*r + 0.587*g + 0.114*b;
}

ReadVideo::ReadVideo(const vector<int> &videos)
{
    for(const int &video: videos){
        //initialise video feeds
        shared_ptr<VideoCapture> newPointer = (shared_ptr<VideoCapture>)(new VideoCapture(video));
        feedArray.push_back(newPointer);
        list<shared_ptr<Mat>> stream;
        syncedStreams.push_back(stream);
    }
}

ReadVideo::ReadVideo(const vector<String> &videos)
{
    for(const String &video: videos){
        //initialise video feeds
        shared_ptr<VideoCapture> newPointer = (shared_ptr<VideoCapture>)(new VideoCapture(video));
        feedArray.push_back(newPointer);
        list<shared_ptr<Mat>> stream;
        syncedStreams.push_back(stream);
    }
}

int ReadVideo::syncStream(){

    size_t NUM_OF_STREAMS = feedArray.size();

    for(size_t i = 0; i < NUM_OF_STREAMS; i++){
        //check if feeds are openable
        if(!feedArray[i]->isOpened())
            return -1;
    }

    double prevLuminence[NUM_OF_STREAMS]; //fixed size array storing luminence of previous frame
    bool streamsHaveSynced[NUM_OF_STREAMS]; //fixed size array storing whether streams have synced

    //initialise values for arrays
    for(size_t i = 0; i < NUM_OF_STREAMS; i++){
        prevLuminence[i] = 0.0;
        streamsHaveSynced[i] = false;
    }

    //iterate through all video frames and find when frames increase in brightness
    for(;;) {
        for(size_t i = 0; i < NUM_OF_STREAMS; i++){
            Mat frame;

            //get a new frame from video stream
            *feedArray[i].get() >> frame;

            //if streams haven't synced yet get rgba from point in video frame and compare if the luminence has increased
            if(!streamsHaveSynced[i]){
                Vec3b colour = frame.at<Vec3b>(Point(frame.rows/2, frame.cols/2));
                double luminence = luminenceCalculator(colour[0], colour[1], colour[2]);
                if((luminence - prevLuminence[i]) >= 15.0 && prevLuminence[i] != 0.0){
                    streamsHaveSynced[i] = true;
                }
                prevLuminence[i] = luminence;
            }
            else{
                syncedStreams[i].push_back((shared_ptr<Mat>)( new Mat(frame) ));
            }
        }

        if(!allStreamsHaveSynced){
            allStreamsHaveSynced = true;
            for(size_t i = 0; i < NUM_OF_STREAMS; i++){
                if(streamsHaveSynced[i] == false){
                    allStreamsHaveSynced = false;
                }
            }
        }

        if(waitKey(30) >= 0) break;
        if(allStreamsHaveSynced) break;
    }

    return 0;
}

vector<shared_ptr<Mat>> ReadVideo::getStreams(){
    vector<shared_ptr<Mat>> streams;
    for(size_t i = 0; i < feedArray.size(); i++){
        //get a new frame from video stream
        Mat frame;
        *feedArray[i].get() >> frame;
        syncedStreams[i].push_back((shared_ptr<Mat>)( new Mat(frame) ));
        auto syncedFrame = syncedStreams[i].front();
        streams.push_back(syncedFrame);
        syncedStreams[i].pop_front();
    }
    return streams;
}

