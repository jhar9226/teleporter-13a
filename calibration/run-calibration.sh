 #!/bin/bash

# run with ./run-calibration.sh {camera name i.e a or b}

# Compile C++
g++ camera_calibration_sample.cpp -o camera_calibration_sample `pkg-config --cflags --libs opencv`;

# Run calibration
./camera_calibration_sample calibrationCalibrator.xml;

# Rename and move file to specified folderi
mv ./out_camera_data.xml ./data-calibration/$1/camera_$1_$(date +"%Y%m%d%H%M")
